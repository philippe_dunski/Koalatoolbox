#ifndef TOOLS_FACTORY_HPP_INCLUDED
#define TOOLS_FACTORY_HPP_INCLUDED
namespace Tools{
template <typename Base>
class Factory{
public:
    template <typename Derived,
              typename ... Args>
    static Base * create(Args ... args){
        return new Derived{args ...};
    }
};
} // namepsace Tools
#endif // TOOLS_FACTORY_HPP_INCLUDED
